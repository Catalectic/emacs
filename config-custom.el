(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-quick-help-delay 1.0)
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#ad7fa8" "#8cc4ff" "#eeeeec"])
 '(auto-indent-key-for-end-of-line-then-newline "<M-RET>")
 '(auto-indent-on-save-file nil)
 '(custom-enabled-themes (quote (solarized-dark)))
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "e16a771a13a202ee6e276d06098bc77f008b73bbac4d526f160faa2d76c1dd0e" "fc6e906a0e6ead5747ab2e7c5838166f7350b958d82e410257aeeb2820e8a07a" default)))
 '(delete-selection-mode t)
 '(font-use-system-font nil)
 '(global-auto-complete-mode t)
 '(global-hi-lock-mode nil)
 '(global-undo-tree-mode t)
 '(global-visual-line-mode t)
 '(helm-adaptative-mode t nil (helm-adaptative))
 '(helm-ff-auto-update-initial-value nil)
 '(ido-ubiquitous-mode t)
 '(iswitchb-mode nil)
 '(kill-whole-line t)
 '(menu-bar-mode t)
 '(scroll-error-top-bottom t)
 '(session-use-package t nil (session))
 '(show-paren-mode t)
 '(smartparens-global-mode t)
 '(sp-highlight-pair-overlay nil)
 '(sp-highlight-wrap-overlay nil)
 '(sp-highlight-wrap-tag-overlay nil)
 '(transient-mark-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:weight extra-bold))))
 '(sp-pair-overlay-face ((((class color) (min-colors 89)) (:background "#eee8d5"))))
 '(sp-wrap-overlay-face ((((class color) (min-colors 89)) (:background "#eee8d5"))))
 '(sp-wrap-tag-overlay-face ((((class color) (min-colors 89)) (:background "#eee8d5")))))
